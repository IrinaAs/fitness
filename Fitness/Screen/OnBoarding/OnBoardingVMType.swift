//
//  OnBoardingViewModel.swift
//  Fitness
//
//  Created by Ирина on 08/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

protocol ViewModelType: class {
    
    func numberOfItems(section: Int) -> Int
    func cellIndexPath(IndexPath: IndexPath) -> ViewCellViewModel?
    
    func createLoginVCTapped()
}


