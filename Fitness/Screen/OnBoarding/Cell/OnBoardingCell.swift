//
//  OnBoardingCell.swift
//  Fitness
//
//  Created by Ирина on 04/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class OnBoardingCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!{
        didSet{
            descriptionLabel.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(0.01)
        }
    }
    
    weak var viewModel: ViewCellViewModelType? {
        willSet (viewModel){
            guard  let viewModel = viewModel else { return }
            imageView.image = viewModel.image
            descriptionLabel.text = viewModel.text
        }
    }
}
