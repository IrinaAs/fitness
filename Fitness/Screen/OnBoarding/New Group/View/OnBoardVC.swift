//
//  OnBoardVC.swift
//  Fitness
//
//  Created by Ирина on 18/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class OnBoardVC: UIViewController {
    
    var viewModel: ViewModel?
    
    @IBOutlet weak var onBoardingCollectionView: UICollectionView!{
        didSet{
            onBoardingCollectionView.dataSource = self
            onBoardingCollectionView.delegate = self
            onBoardingCollectionView.register(UINib(nibName: "OnBoardingCell", bundle: Bundle.main), forCellWithReuseIdentifier: "onBoardingCell")
        }
    }
    
    @IBOutlet weak var carouselPageControl: UIPageControl!{
        didSet{
            carouselPageControl.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(0.01)
        }
    }
    
    @IBOutlet weak var nextButton: UIButton!{
        didSet{
            nextButton.layer.cornerRadius = 12
            nextButton.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(0.1)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ViewModel()
    }
    
    @IBAction func didButtonClicked(_ sender: UIButton) {
        
        if (self.carouselPageControl.currentPage == 0) ||
            (self.carouselPageControl.currentPage == 1) ||
            (self.carouselPageControl.currentPage == 2) {
            
            let collectionBounds = self.onBoardingCollectionView.bounds
            let contentOffset = CGFloat(self.onBoardingCollectionView.contentOffset.x + collectionBounds.size.width)
            self.moveCollectionToFrame(contentOffset: contentOffset)
        } else {
            viewModel?.createLoginVCTapped()
        }
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x: contentOffset, y: self.onBoardingCollectionView.contentOffset.y, width: self.onBoardingCollectionView.frame.width, height: self.onBoardingCollectionView.frame.height)
        self.onBoardingCollectionView.scrollRectToVisible(frame, animated: true)
    }
}

extension OnBoardVC: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {return 0}
        self.carouselPageControl.numberOfPages = viewModel.numberOfItems(section: section)
        return viewModel.numberOfItems(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = onBoardingCollectionView.dequeueReusableCell(withReuseIdentifier: "onBoardingCell", for: indexPath) as? OnBoardingCell else { return UICollectionViewCell() }
    
        guard let viewModel = viewModel else { return UICollectionViewCell() }
       let collectionViewCellModel = viewModel.cellIndexPath(IndexPath: indexPath)
        cell.viewModel = collectionViewCellModel
        return cell
    }
}

extension OnBoardVC: UICollectionViewDelegate,  UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    //Запрашивает у делегата размер ячейки указанного элемента
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.onBoardingCollectionView.frame.size.width, height: self.onBoardingCollectionView.frame.size.height)
    }

    //Получаем путь элемента при scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.tag == 0) {
            let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width/2), y: scrollView.frame.height/2)
            if let indexPath = self.onBoardingCollectionView.indexPathForItem(at: center) {
                self.carouselPageControl.currentPage = indexPath.row
            }
        }
    }
}

//extension OnBoardVC: OnBoardingView{
//
//    var _navigationController: UINavigationController? {
//        return self.navigationController
//    }
//
//}
