//
//  DataOnBoarding.swift
//  Fitness
//
//  Created by Ирина on 04/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class OnBoardingViewModel{
    
    var items: [OnBoardingItem] = [OnBoardingItem(descriptionTour: "Добро пожаловать! Вы установили приложение Адреналин. Для знакомства с функционалом приложения просмотрите ознакомительный тур.",
                                                  imageTour: UIImage(named: "onBoarding7")!),
                                   
                                   OnBoardingItem(descriptionTour: "Каждое занятие имеет свое уникальное цветовое обозначение для удобства.",
                                                  imageTour: UIImage(named:"onBoarding8")!),
                                   
                                   OnBoardingItem(descriptionTour: "Каждое упражнение содержит максимальную  информацию для ее выполнения.",
                                                  imageTour: UIImage(named:"onBoarding5")!),
                                   
                                   OnBoardingItem(descriptionTour: "Новый день сопровождается цитатой великих людей.",
                                                  imageTour: UIImage(named:"onBoarding1-1")!)
    ]
}

class OnBoardingItem {
    
    var descriptionTour: String
    var imageTour: UIImage
    
    init(descriptionTour: String, imageTour: UIImage) {
        self.imageTour = imageTour
        self.descriptionTour = descriptionTour
    }
}
