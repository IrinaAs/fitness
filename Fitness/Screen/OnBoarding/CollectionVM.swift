//
//  ViewController.swift
//  Fitness
//
//  Created by Ирина on 18/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class ViewModel: ViewModelType {
    
    var item = OnBoardingViewModel().items
    
    func numberOfItems(section: Int) -> Int {
        return item.count
    }
    
    func cellIndexPath(IndexPath: IndexPath) -> ViewCellViewModel? {
        let item = self.item[IndexPath.row]
        return ViewCellViewModel(item: item)
    }
    
    func createLoginVCTapped() {
    }
    
}
