//
//  OnBoardingPresenter.swift
//  Fitness
//
//  Created by Ирина on 08/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

protocol ViewCellViewModelType: class {
    
    var image: UIImage { get }
    var text: String { get }
}

class ViewCellViewModel: ViewCellViewModelType {
    
    private var item: OnBoardingItem
    
    var image: UIImage{
        return item.imageTour
    }
    
    var text: String{
        return String(describing: item.descriptionTour)
    }
    
    init(item: OnBoardingItem){
        self.item = item
    }
}



