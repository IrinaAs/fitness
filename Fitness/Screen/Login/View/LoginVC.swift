//
//  LoginVC.swift
//  Fitness
//
//  Created by Ирина on 04/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginVC: UIViewController {
    
    //    var ref: DatabaseReference!
    var presenter: LoginViewPresenter?
    
    @IBOutlet weak var warningLabel: UILabel!{
        didSet{
            warningLabel.textColor = #colorLiteral(red: 0.865183413, green: 0.8100412488, blue: 0.7633895278, alpha: 1)
            warningLabel.font = UIFont(name: "Sistem", size: 25)
        }
    }
    
    @IBOutlet weak var exitButton: UIButton!{
        didSet{
            exitButton.layer.cornerRadius = 12
            exitButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.1)
        }
    }
    
    @IBOutlet weak var registrationButton: UIButton!{
        didSet{
            registrationButton.layer.cornerRadius = 12
            registrationButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.2)
        }
    }
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = loadFromNib()
        //        ref = Database.database().reference(withPath: "users")
        //        listen()
        warningLabel.alpha = 0
        
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "LoginVC", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        hideKeyboardWhenTappedAround()
        emailTextField.text = ""
        passwordTextField.text = ""
        
    }

    
    //    func listen(){
    //        Auth.auth().addIDTokenDidChangeListener({ [weak self] (auth, user) in
    //            if user != nil {
    //                self?.performSegue(withIdentifier: "taskSegue", sender: nil)
    //            }
    //        })
    //    }

//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }

    func displayWarningLabel(text: String) {
        warningLabel.text = text
        
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            [weak self] in
            self?.warningLabel.alpha = 1
        }) { [weak self] completion in
            self?.warningLabel.alpha = 0
        }
    }
    
    
    @IBAction func clickOnExit(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text, email != "", password != "" else {
            displayWarningLabel(text: "Пожалуйста, введите e-mail и пароль.")
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] (user, error) in
            if error != nil {
                self?.displayWarningLabel(text: "Ошибка! Пожалуйста, проверьте логин или пароль.")
                return
            }
            if  user != nil {
                self?.performSegue(withIdentifier: "taskSegue", sender: nil)
                return
            }
            self?.displayWarningLabel(text: "Пользователь незарегистрирован")
        }
    }
    
    
    @IBAction func clickOnRegistration(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text, email != "", password != "" else {
            displayWarningLabel(text: "Пожалуйста, введите e-mail и пароль.")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password, completion: { [weak self] (user, error) in
            
            //            if error == nil {
            //                if user != nil {
            //                    //                self?.performSegue(withIdentifier: "taskSegue", sender: nil)
            //                } else {
            //                    print("user is not created")
            //                }
            //            } else {
            //                print(error?.localizedDescription ?? "")
            
            //
            //            }
            //        })
            //    }
            guard error == nil, user != nil else {
                print(error!.localizedDescription)
                return
            }
            
            
            //            let user = (user)?.user
            //            let userRef = self?.ref.child(user!.uid)
            //            userRef?.setValue(["email" : user?.email])
        })
    }
    
}

extension LoginVC: LoginView{
    
    func setName(value: String) {
    }
    
}
