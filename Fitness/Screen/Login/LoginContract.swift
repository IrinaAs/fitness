//
//  LoginContract.swift
//  Fitness
//
//  Created by Ирина on 09/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

protocol LoginViewPresenter{
    
    func signInButtonTapped()
}

protocol LoginView: CommonViewProtocol{
    
    func setName(value: String)
    
}
