//
//  MainCoordinator.swift
//  Fitness
//
//  Created by Ирина on 08/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class AllCoordinators: Coordinator {
    
    private let presenter: UINavigationController
    private var loginVC: LoginVC?
    private var onBoardingVC: OnBoardVC?
    
    init(presenter: UINavigationController){
        self.presenter = presenter
    }
    
    func start() {
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        presenter.pushViewController(loginVC, animated: true)
        self.loginVC = loginVC
    }
    
    func startOnBoarding() {
        let onBoardingVC = OnBoardVC(nibName: "OnBoardVC", bundle: nil)
        presenter.pushViewController(onBoardingVC, animated: true)
        self.onBoardingVC = onBoardingVC
    }
}
