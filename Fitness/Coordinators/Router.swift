//
//  Router.swift
//  Fitness
//
//  Created by Ирина on 11/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import Foundation
import UIKit

//Cтандартные переходы
protocol Router {
    
    var presenter: UINavigationController? {get}
    func present(controller: UIViewController, animated: Bool)
    func push(controller: UIViewController, animated: Bool)
    func popController(animated: Bool)
    func dismissController(animated: Bool)
    
}

extension Router {
    
    func present(controller: UIViewController, animated: Bool){
        presenter?.present(controller, animated: animated, completion: nil)
    }
    
    func push(controller: UIViewController, animated: Bool){
        presenter?.pushViewController(controller, animated: animated)
    }
    
    func popController(animated: Bool = true ){
        presenter?.popViewController(animated: animated)
    }
    
    func dismissController(animated: Bool = true ){
        presenter?.dismiss(animated: animated, completion: nil)
    }
    
}
