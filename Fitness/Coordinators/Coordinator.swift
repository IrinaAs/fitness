//
//  Coordinator.swift
//  Fitness
//
//  Created by Ирина on 11/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//


protocol Coordinator {
    
    func start()
}
    
 
