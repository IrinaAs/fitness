//
//  BasePresenter.swift
//  Fitness
//
//  Created by Ирина on 08/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

class BasePresenter<Model, View, Coord>{
    
    var coordinator: Coord?
    let view: View
    var model: Model
    
    required init(view: View, model: Model) {
        self.view = view
        self.model = model
    }
    
}

class BaseViewModel <Model, View>{
    
    let view: View
    var model: Model
    
    required init(view: View, model: Model) {
        self.view = view
        self.model = model
        setup()
    }
    
    func setup(){
        
    }
}
