//
//  CommonViewProtocol.swift
//  Fitness
//
//  Created by Ирина on 08/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//


import UIKit

protocol CommonViewProtocol: class {
    
    func showLoading()
    func showLoading(text: String)
    func hideLoading()
    func showError(message: String)

}

extension CommonViewProtocol where Self: UIViewController {
    
    func showLoading(){}
    func showLoading(text: String){}
    func hideLoading(){}
    func showError(message: String){}
}
