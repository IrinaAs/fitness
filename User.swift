//
//  User.swift
//  Fitness
//
//  Created by Ирина on 05/07/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import Firebase
import Firebase
import FirebaseDatabase

struct Users {
    
    let uid: String
    let email: String
    
//    init(user: User) {
//        self.uid = user.uid
//        self.email = user.email!
//    }
}


struct Task {
    let title: String
    let userId: String
    let ref: DatabaseReference?
    var completed: Bool = true
    
    init(title: String, userId: String){
        self.title = title
        self.userId = userId
        self.ref = nil
    }
    
    init(shapshot: DataSnapshot){
        let shapshotValue = shapshot.value as! [String: Any]
        title = shapshotValue["title"] as! String
        userId = shapshotValue["userId"] as! String
        completed = shapshotValue["completed"] as! Bool
        ref = shapshot.ref
    }
    
    func convertToDictionary() -> Any {
        return ["title" : title, "userId" : userId, "completed" : completed]
    }
}
